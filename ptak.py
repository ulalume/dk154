#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Vykreslí graf ptáka s vyznačenými poli v okamžiku jejich pozorování."""


from __future__ import print_function

import sys
from lxml.html import parse
from restriction import restriction, check, main, reverse
from matplotlib import pyplot as plt


def schtab(html_file):
    """Vyparsuje potřebné informace ze souboru se schedule."""

    html = parse(html_file).getroot()
    table = html.xpath("//table")[1]
    rows = table.xpath("//tr")

    ra = []
    de = []
    ut = []

    for row in rows[2:]:
        cols = row.xpath("td")
        ra.append(cols[3].text.strip())
        de.append(cols[4].text.strip())
        ut.append(cols[10].text_content().strip())

    return ra, de, ut


if len(sys.argv) != 3:
    sys.exit("Použití: python ptak.py DATE FILE.html")
else:
    date = sys.argv[1]
    html_file = sys.argv[2]


RA, DE, UT = schtab(html_file)

# vykresleni grafu do obrazku
plt.title("Pointing restriction\n" + date)
plt.xlabel("Hour angle")
plt.ylabel("Declination")
plt.plot(main[:,0], main[:,1], color="blue", label="main")
plt.fill_between(main[:,0], main[:,1], -90, color="blue", alpha=0.5)
plt.plot(reverse[:,0], reverse[:,1], color="green", label="reverse")
plt.fill_between(reverse[:,0], reverse[:,1], -90, color="green", alpha=0.5)
for i in range(len(RA)):
    point = restriction(date, UT[i], RA[i], DE[i])
    plt.plot(point.x, point.y, "ro")
    plt.annotate(i+1, (point.x, point.y))
    print("{:2d}. {}".format(i+1, check(point)))
plt.xlim([-180, 180])
plt.xticks(range(-180, 181, 30))
plt.ylim([-90, 60])
plt.yticks(range(-90, 61, 15))
plt.legend()
plt.grid()
plt.savefig("ptak.png")


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
