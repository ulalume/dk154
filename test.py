import mainreversecontrol as mrc
import numpy as np
import matplotlib.pylab as plt


ra = '9:16:01.66'
dec = '-36:37:38.76'

datetime = '2014/02/15 08:53:00'

pozorovani = mrc.DK154Observation(datetime,ra,dec)

# LHA in unit [rads]

lha,dec_deg,alt = pozorovani.object_position()

print lha,dec_deg,alt

lha_hour = pozorovani.rads_to_hours_24(lha)
lha_deg = pozorovani.hours_24_to_deg(lha_hour)



if lha_deg > 270.0:
    lha_deg = lha_deg - 360.0



# Altitude in unit [deg]

alt = pozorovani.deg_mm_ss_to_deg(str(alt))

print "Local Hour Angle: ", lha_hour," [h] ", lha_deg," [deg]"
print "Declination: ",dec_deg," [deg] "
print "Altitude : ", alt," [deg] "

# Main and Reverse position

pozice =  mrc.DK154TelescopeModel()
print 'H.A ',lha_deg,' D.A ',dec_deg
print 'Main pozice: ',pozice.main_position(lha_deg,dec_deg)

print 'Reverse pozice: ',pozice.reverse_position(lha_deg,dec_deg)

# Print position in the plot

main= np.genfromtxt('main.dat')
reverse = np.genfromtxt('reverse.dat')



plt.title('Telescope position')

plt.plot(main[:,0],main[:,1],'b-')
plt.plot(reverse[:,0],reverse[:,1],'b-')

lha_deg_reverse = pozice.deg_to_360(lha_deg + 180.0)
dec_deg_reverse = pozice.deg_to_360(-dec_deg - 180.0)

if lha_deg_reverse > 270:
    lha_deg = lha_deg - 360.0

p1,=plt.plot(lha_deg,dec_deg,'bo',label='Main')
p2,=plt.plot(lha_deg_reverse,dec_deg_reverse,'ro',label='Reverse')

plt.savefig('plot.png',dpi=None,format='png')

plt.show()