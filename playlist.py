#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Pro kazdy radek vstupniho souboru vyrobi sekvenci pozorovani ve vsech
filtrech a jeste jednou s posunutim o ~deset uhlovych vterin v kazdem smeru."""


from __future__ import print_function, division

import sys
from astropy.coordinates import SkyCoord


# pozorovaci sekvence
sekvence = {
    "normal": {  # standardni
        "R": "60",
        "V": "60",
        "I": "60",
        "B": "120",
        "U": "600"
    },
    "short": {  # zkracena
        "R": "30",
        "V": "30",
        "I": "30",
        "B": "60",
        "U": "300"
    },
    "NGC3293": {
        "R": "30",
        "V": "30",
        "I": "30",
        "B": "40",
        "U": "400"
    }
}

# filtrove kolo
wheel = ["R", "V", "I", "B", "U"]


def parser(ra, dec):
    """Rozparsuje retezce do souradnic."""
    ra = ra.strip()
    ra_str = ra[0:2] + "h" + ra[2:4] + "m" + ra[4:9] + "s"
    dec = dec.strip()
    if dec.startswith("-"):
        dec_str = dec[0:3]
        dec = dec.lstrip("-")
    else:
        dec_str = dec[0:2]
    dec_str += "d" + dec[2:4] + "m" + dec[4:9] + "s"
    return SkyCoord(ra=ra_str, dec=dec_str, frame='icrs')


def add_offset(coor):
    """Prida offset k souradnicim."""
    coor.ra.hourangle += 15 / 60 / 60 / 15  # 1 sekunda
    coor.dec.degree += 10 / 60 / 60         # 10 vterin
    return coor


def to_strings(coor):
    """Prevede souradnice zpet na retezce."""
    ra = coor.ra.to_string(unit="hour", sep="", precision=2)
    dec = coor.dec.to_string(sep="", precision=2)
    return [ra, dec]


def sekvenzer(radek, offset=False, plan=sekvence["normal"]):
    """Z Radka extrahuje potrebne informace ze zaznamu a vytvori z nich sekvenci
    pro pozorovani. V pripade potreby i s posunutymi souradnicemi."""
    field = radek[0]
    telpos = radek[4]
    ra = radek[5]
    dec = radek[6]
    coor = parser(ra, dec)
    if offset is True:
        coor = add_offset(coor)
    ra, dec = to_strings(coor)
    for filtr in wheel:
        record = []
        record.append(field)                  # field
        record.append(filtr.rjust(2))         # filtr
        record.append(plan[filtr].rjust(4))   # exptime
        record.append("1".rjust(2))           # count
        record.append(telpos)                 # telpos
        record.append(ra.zfill(9).rjust(10))  # rektascenze
        if dec.startswith("-"):                # deklinace
            record.append(dec.zfill(10).rjust(11))
        else:
            record.append(dec.zfill(9).rjust(11))
        record.append("0".rjust(2))           # delay
        print(";".join(record))


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("Použití: python playlist.py SCHTEXT.txt")
    else:
        vstup = open(sys.argv[1], "r").readlines()

    for radek in vstup:
        radek = radek.rstrip("\n").split(";")
        if radek[0].strip() in sekvence:
            sekvenzer(radek, offset=False, plan=sekvence[radek[0].strip()])
            sekvenzer(radek, offset=True, plan=sekvence[radek[0].strip()])
        else:
            sekvenzer(radek, offset=False, plan=sekvence["normal"])
            sekvenzer(radek, offset=True, plan=sekvence["normal"])
        print("")


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
