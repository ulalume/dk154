import ephem
import coords as C
import time
import math
import re
from shapely.geometry import Polygon
from shapely.geometry import Point

class  DK154Observation:

    def __init__(self,datetime, ra, dec):

        # Geographical coordinates of the La Silla, Chile
        self.latitude = '-29:15:39.5'
        self.longitude = '-70:43:54.1'
        self.elevation = 2400

        # Date and time of observation
        self.datetime = datetime
        # Right ascension and declination of the observed object
        self.ra = ra
        self.dec = dec

# convert declination from format xx:xx:xx.xx to [deg]
    def declination_to_deg(self,dec):
	pattern = r'(-*\d*):(\d*):(\d*.\d*)'
	match = re.search(pattern,str(dec))

	if match:
	    deg = match.group(1)
	    mm = match.group(2)
	    ss = match.group(3)
	    if float(deg) < 0.0:
		dec_deg = -1.0*(abs(float(deg)) + (float(mm) + float(ss)/60.0)/60.0)
	    else:
		dec_deg = float(deg) + (float(mm) + float(ss)/60.0)/60.0
	else:
	    print " Invalid input format of declination "
	    dec_deg = 999.0

	return dec_deg

    # Assume input in the format of altitude dd:mm:ss
    def deg_mm_ss_to_deg(self,alt_dgms):
	pattern = r'(\d*):(\d*):(\d*.\d*)'
	match = re.search(pattern,str(alt_dgms))
	if match:
	    deg = match.group(1)
	    mm = match.group(2)
	    ss = match.group(3)
	else:
	    print "Invalid input format of altitude : [dd:mm:ss]"

	alt_deg = float(ss)/60.+float(mm)/60.+float(deg)
	return alt_deg

    # Transformation of angle in [hours] 24-format to angle in [deg]
    def hours_24_to_deg(self,lha_hours):
	lha_deg = lha_hours*15.0
	return lha_deg

    # Transformation of angle in [rads] to angle in [hours] 24-format
    def rads_to_hours_24(self,lha_rads):
	lha_hours = lha_rads/(math.pi/12.)
	if lha_hours < 0:
	    lha_hours = 24.0 + lha_hours 
	else:
	    lha_hours = lha_hours
	return lha_hours

    # Calculate object position for La Silla
    # Namely: 
    #		Local Hour Angle [rads]
    #		Declination	[deg]
    #		Altitude	[deg]

    def object_position(self):
        observer = ephem.Observer()
        observer.lat = self.latitude
        observer.lon = self.longitude
        observer.elevation = self.elevation
	observer.date = self.datetime

        field = ephem.FixedBody()
        field._ra = self.ra
        field._dec = self.dec

        # Rovnikove souradnice I. druhu

        # Local Hour Angle : units [rads] range (-12,12)
        lha = observer.sidereal_time() - field._ra

	# Declination : units [deg]
	
	da = self.declination_to_deg(self.dec)

        field.compute(observer)

        # Altitude: units [deg:mm:ss]
        alt = field.alt

        return lha, da, alt, 

class DK154TelescopeModel:

    def __init__(self):

	# list of boundary points of restricted area for main and reverse position
	self.boundary_set_main = Polygon(((-18.00,-90.00), (-20.00,-38.00), (-30.00, -37.50), \
            (-40.00, -37.00), (-50.00, -36.50), (-63.00, -36.00), (-63.00, -30.50), \
            (-60.00, -29.00), (-50.00, -12.00), (-40.00, 12.00), (-30.00, 26.00), \
            (-20.00, 35.50), (-10.00, 42.00), (0.00, 45.74), (10.00, 45.18), \
            (20.00, 43.46), (30.00, 40.38), (40.00, 35.60), (50.00, 28.56), \
            (60.00, 18.48), (70.00, 4.54), (80.00, -13.16), (90.00, -13.16), \
            (100.00, -47.68), (110.00, -58.27), (120.00, -65.01), (130.00, -69.28), \
            (140.00, -72.03), (150.00, -73.81), (160.00, -74.93), (170.00, -75.54), \
            (180.00, -75.74), (190.00, -75.74), (198.00, -88.00), (198.00, -90.00)))

        self.boundary_set_reverse = Polygon(((200.00, -110.00), (210.00, -110.50), \
    	    (220.00, -111.00), (230.00, -115.00), (240.00, -130.00), (240.00, -151.25), \
    	    (237.00, -155.00), (229.00, -165.00), (225.00, -170.00), (220.00, -174.00), \
    	    (210.00, -180.00), (205.00, -182.00), (190.00, -225.19), (180.00, -225.74), \
    	    (170.00, -225.18), (160.00, -223.46), (150.00, -220.38), (140.00, -215.60), \
    	    (130.00, -208.56), (120.00, -198.47), (110.00, -184.53), (100.00, -166.83), \
    	    (90.00, -148.02), (80.00, -132.40), (70.00, -121.73), (60.00, -114.98), \
    	    (50.00, -110.72), (40.00, -107.97), (30.00, -106.18), (20.00, -105.07), 
    	    (10.00, -104.46), (0.00, -104.46), (-10.00, -104.26)))

#	# Method for determine position of the object and return True if object position
	# is inside observeable area according to Main and Reverse position

    # Angle in range (0,360) 
    def deg_to_360(self,deg):
	if deg > 360.0:
	    deg = deg - 360
	return deg

    def is_it_observeable(self,object_lha,object_dec):
    
	return observeable
	
    def main_position(self,object_lha,object_dec):
	
	area_main = self.boundary_set_main
	point = Point(object_lha,object_dec)
	main = area_main.contains(point)
	
	return main
	
    def reverse_position(self,object_lha,object_dec):
	# Transformation of object coordinates x = LHA [deg] , y = Declination [deg]
	# x_trans = x + 180
	# y_trans = y - 180
	x_trans = self.deg_to_360(object_lha + 180)
	y_trans = self.deg_to_360(-object_dec - 180)
	
	area_reverse = self.boundary_set_reverse
	
	point = Point(x_trans,y_trans)
	reverse = area_reverse.contains(point)
	
	return reverse