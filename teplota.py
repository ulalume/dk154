#!/usr/bin/env python
"""Pro zadané datum vykreslí teplotu CCD během dané pozorovací noci.
   Výsledek uloží do souboru teplota_RRRR-MM-DD.png"""


from __future__ import print_function

import os
import sys
import glob
import numpy as np
from astropy.io import fits as pf
from matplotlib import pyplot as plt


if len(sys.argv) != 2:
    sys.exit("Použití: python teplota.py RRRR-MM-DD")
else:
    date = sys.argv[1]

path = os.path.join("/i/archdata/DK154.RAW/", date.replace("-", ""))

if not os.path.isdir(path):
    print(path, file=sys.stderr)
    sys.exit("Zdá se, že zadaný adresář neexistuje :-(")
else:
    mask = os.path.join(path, "*.fits")
    fits = glob.glob(mask)

jd = []
temp = []

for f in fits:
    jd.append(pf.getval(f, "JD"))
    temp.append(pf.getval(f, "CCD_TEMP"))

JD = np.floor(jd[0])
jd = np.array(jd)
MJD = jd - JD

temp = np.array(temp)
temp_mean = temp.mean()
temp_std = temp.std()

plt.title("Teplota CCD {}".format(date))
plt.xlabel("JD - {}".format(JD))
plt.ylabel("CCD_TEMP")
plt.axhspan(ymin=temp_mean-1*temp_std, ymax=temp_mean+1*temp_std, color="g", alpha=0.8)
plt.axhspan(ymin=temp_mean-2*temp_std, ymax=temp_mean+2*temp_std, color="g", alpha=0.6)
plt.axhspan(ymin=temp_mean-3*temp_std, ymax=temp_mean+3*temp_std, color="g", alpha=0.4)
plt.axhline(y=temp_mean, lw=2, color="r", zorder=1)
plt.scatter(MJD, temp, zorder=2)
plt.grid()
plt.savefig("teplota_{}.png".format(date))

print("Obrázek uložen do souboru teplota_{}.png".format(date))


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
