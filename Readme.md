# DK154

Podpůrné skripty pro přípravu pozorování s robotickým teleskopem DK154 na observatoři La Silla v Chile.

## Závislosti

* [astropy](http://www.astropy.org/)
* [Shapely](https://pypi.python.org/pypi/Shapely)
* [lxml](http://lxml.de/)

## Další čtení

* [Wiki](dk154/wiki) - Nepřehlédněte důležité informace zdarma a pro všechny!
