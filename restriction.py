#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Ověří polohu dalekohledy pro zadaný čas pozorování a souřadnice na obloze.
A aby toho nebylo málo, nakreslí i pěkný obrázek"""


from __future__ import print_function

import sys
from astropy.time import Time
from astropy.coordinates import EarthLocation, Latitude, Longitude
from astropy import units as u
from numpy import array  # , loadtxt
from matplotlib import pyplot as plt
from shapely.geometry import Point
from shapely.geometry import Polygon


# hranice oblasti MAIN
# main = loadtxt("main.dat")
main = array([
    [ -18.  ,  -90.  ],
    [ -20.  ,  -38.  ],
    [ -30.  ,  -37.5 ],
    [ -40.  ,  -37.  ],
    [ -50.  ,  -36.5 ],
    [ -63.  ,  -36.  ],
    [ -63.  ,  -30.5 ],
    [ -60.  ,  -29.  ],
    [ -50.  ,  -12.  ],
    [ -40.  ,   12.  ],
    [ -30.  ,   26.  ],
    [ -20.  ,   35.5 ],
    [ -10.  ,   42.  ],
    [   0.  ,   45.74],
    [  10.  ,   45.18],
    [  20.  ,   43.46],
    [  30.  ,   40.38],
    [  40.  ,   35.6 ],
    [  50.  ,   28.56],
    [  60.  ,   18.48],
    [  70.  ,    4.54],
    [  80.  ,  -13.16],
    [  90.  ,  -31.97],
    [ 100.  ,  -47.6 ],
    [ 110.  ,  -58.27],
    [ 120.  ,  -65.01],
    [ 130.  ,  -69.28],
    [ 140.  ,  -72.03],
    [ 150.  ,  -73.81],
    [ 160.  ,  -74.93],
    [ 170.  ,  -75.54],
    [ 180.  ,  -75.74],
    [ 190.  ,  -75.74],
    [ 198.  ,  -88.  ],
    [ 198.  ,  -90.  ]
])

MAIN = Polygon(main)

# hranice oblasti REVERSE
# reverse = loadtxt("reverse.dat")
# reverse *= [1, -1]
# reverse -= [180, 180]
reverse = array([
    [-198.  ,  -90.  ],
    [-190.  ,  -75.74],
    [-180.  ,  -75.54],
    [-170.  ,  -75.54],
    [-160.  ,  -74.93],
    [-150.  ,  -73.82],
    [-140.  ,  -72.03],
    [-130.  ,  -69.28],
    [-120.  ,  -65.02],
    [-110.  ,  -58.27],
    [-100.  ,  -47.6 ],
    [ -90.  ,  -31.98],
    [ -80.  ,  -13.17],
    [ -70.  ,    4.53],
    [ -60.  ,   18.47],
    [ -50.  ,   28.56],
    [ -40.  ,   35.6 ],
    [ -30.  ,   40.38],
    [ -20.  ,   43.46],
    [ -10.  ,   45.18],
    [   0.  ,   45.74],
    [  10.  ,   45.19],
    [  25.  ,    2.  ],
    [  30.  ,    0.  ],
    [  40.  ,   -6.  ],
    [  45.  ,  -10.  ],
    [  49.  ,  -15.  ],
    [  57.  ,  -25.  ],
    [  60.  ,  -28.75],
    [  60.  ,  -50.  ],
    [  50.  ,  -65.  ],
    [  40.  ,  -69.  ],
    [  30.  ,  -69.5 ],
    [  20.  ,  -70.  ],
    [  18.  ,  -90.  ]])

REVERSE = Polygon(reverse)


def restriction(date, time, ra, dec):
    """Spočte polohu pozorovaneho pole v ptakovi."""

    # misto pozorovani
    deka = EarthLocation(lat="-29:15:39.5", lon="-70:43:54.1", height=2400)

    # cas pozorovani
    time = Time(date + " " + time, location=deka)
    time.delta_ut1_utc = 0  # IERS workaround

    # pozorovany objekt
    ra = Longitude(ra, unit=u.hourangle)
    dec = Latitude(dec, unit=u.degree)

    # hodinovy uhel
    ha = (time.sidereal_time("apparent") - ra).wrap_at("180d")

    # pozorovany bod
    point = Point(ha.degree, dec.degree)

    return point


def check(point):
    """Ověří polohu porozovaneho pole v ptakovi."""

    if point.within(MAIN):
        return "MAIN"
    elif point.within(REVERSE):
        return "REVERSE"
    else:
        return "NONE"


if __name__ == "__main__":

    if len(sys.argv) != 5:
        sys.exit("Použití: python restriction.py DATE TIME RA DEC")
    else:
        date = sys.argv[1]
        time = sys.argv[2]
        ra = sys.argv[3]
        dec = sys.argv[4]

    point = restriction(date, time, ra, dec)

    # informativni vypis
    print("\n{:^80}\n".format(check(point)))

    # vykresleni grafu do obrazku
    plt.title("Pointing restriction\n" + date + " " + time)
    plt.xlabel("Hour angle")
    plt.ylabel("Declination")
    plt.plot(main[:,0], main[:,1], color="blue", label="main")
    plt.fill_between(main[:,0], main[:,1], -90, color="blue", alpha=0.5)
    plt.plot(reverse[:,0], reverse[:,1], color="green", label="reverse")
    plt.fill_between(reverse[:,0], reverse[:,1], -90, color="green", alpha=0.5)
    plt.plot(point.x, point.y, "ro")
    plt.xlim([-180, 180])
    plt.xticks(range(-180, 181, 30))
    plt.ylim([-90, 60])
    plt.yticks(range(-90, 61, 15))
    plt.legend()
    plt.grid()
    plt.savefig("restriction.png")


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
